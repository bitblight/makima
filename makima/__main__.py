
import argparse
import datetime
import itertools
import logging
import json
import re
import tabulate
import zipfile

JSON_FILENAME_TEMPLATE = r'\A\d+_{dataset}.json\Z'

FUN_RIGHTS = [
    'AddAllowedToAct',
    'AddKeyCredentialLink',
    'AddMember',
    'AddSelf',
    'AllExtendedRights',
    'AllowedToAct',
    'AdminTo',
    'AllowedToDelegate',
    'CanPSRemote',
    'CanRDP',
    'DCSync',
    'ForceChangePassword',
    'GenericAll',
    'GenericWrite',
    'Owns',
    'ReadGMSAPassword',
    'ReadLAPSPassword',
    'SQLAdmin',
    'SyncLAPSPassword',
    'WriteAccountRestrictions',
    'WriteDacl',
    'WriteOwner',
    'WriteSPN'
]


class BloodhoundSchemaError(ValueError):

    pass


class BloodhoundACE(object):

    def __delitem__(self, key):

        del self.json[key]

    def __getitem__(self, key):

        return self.json[key]

    def __init__(self, obj):

        self.json = obj
    
    def __repr__(self):

        return f"<{__name__}.{self.__class__.__name__} sid={self.sid} right={self.right}>"
    
    def __setitem__(self, key, new_value):

        self.json[key] = new_value
    
    @property
    def principal_type(self):

        return self.json.get('PrincipalType')
    
    @property
    def right(self):

        return self.json.get('RightName')
    
    @property
    def sid(self):

        return self.json.get('PrincipalSID')


class BloodhoundObject(object):

    # TODO: verify that the times are actually UTC

    def __delitem__(self, key):

        del self.json[key]

    def __getitem__(self, key):

        return self.json[key]

    def __init__(self, obj):

        self.json = obj

    def __repr__(self):

        return f"<{__name__}.{self.__class__.__name__} oid={self.oid} name='{self.name}'>"
    
    def __setitem__(self, key, new_value):

        self.json[key] = new_value
    
    @property
    def aces(self):

        try:
            return [BloodhoundACE(ace) for ace in self.json['Aces']]
        except KeyError:
            raise BloodhoundSchemaError()
    
    @property
    def admin_count(self):

        # TODO: boolean
        return self.properties.get('admincount')
    
    @property
    def created(self):

        # TODO: handle case where whencreated is not set
        return datetime.datetime.utcfromtimestamp(self.properties['whencreated'])
    
    @property
    def description(self):

        try:
            return self.properties.get('description')
        except KeyError:
            return None
    
    @property
    def display_name(self):

        return self.properties.get('displayname')
    
    @property
    def distinguished_name(self):

        return self.properties.get('distinguishedName')
    
    @property
    def domain(self):

        return self.properties.get('domain')
    
    @property
    def domain_sid(self):
        
        return self.properties.get('domainsid')
    
    @property
    def email(self):

        return self.properties.get('email')
    
    @property
    def enabled(self):

        # TODO: boolean
        return self.properties.get('enabled')
    
    @property
    def has_laps(self):

        # TODO: boolean
        has_laps = self.properties.get('haslaps')
        if has_laps is None:
            raise ValueError()
        return has_laps
    
    @property
    def has_spn(self):

        # TODO: boolean
        return self.properties.get('hasspn')
    
    @property
    def home_directory(self):

        return self.properties.get('homedirectory')
    
    @property
    def is_acl_protected(self):

        return self.json.get('IsACLProtected')
    
    @property
    def is_deleted(self):

        return self.json.get('IsDeleted')
    
    @property
    def last_logon(self):

        # TODO: handle case where whencreated is not set
        return datetime.datetime.utcfromtimestamp(self.properties['lastlogon'])
    
    @property
    def last_logon_timestamp(self):

        # TODO: handle case where whencreated is not set
        return datetime.datetime.utcfromtimestamp(self.properties['lastlogontimestamp'])
    
    @property
    def logon_script(self):

        return self.properties.get('logonscript')
    
    @property
    def name(self):

        return self.properties.get('name')
    
    @property
    def oid(self):

        try:
            return self.json['ObjectIdentifier']
        except KeyError:
            raise BloodhoundSchemaError()
    
    @property
    def operating_system(self):

        return self.properties.get('operatingsystem')
    
    @property
    def password_last_set(self):

        # TODO: handle case where whencreated is not set
        return datetime.datetime.utcfromtimestamp(self.properties['pwdlastset'])
    
    @property
    def password_never_expires(self):

        # TODO: if this isn't set it will return none, breaking the boolean
        # contract
        return self.properties.get('pwdneverexpires')
    
    @property
    def password_not_required(self):

        # TODO: boolean
        return self.properties.get('passwordnotreqd')
    
    @property
    def properties(self):

        try:
            return self.json['Properties']
        except KeyError:
            raise BloodhoundSchemaError()
    
    @property
    def sam_account_name(self):

        return self.properties.get('samaccountname')
    
    @property
    def sensitive_for_delegation(self):

        # TODO: boolean
        return self.propertes.get('sensitive')
    
    @property
    def spns(self):

        return self.properties.get('serviceprincipalnames')
    
    @property
    def title(self):

        return self.properties.get('title')
    
    @property
    def trusted_to_auth_for_delegation(self):

        # TODO: boolean
        return self.properties.get('trustedtoauth')
    
    @property
    def unconstrained_delegation_configured(self):

        return self.properties.get('unconstraineddelegation')
    
    @property
    def unicodepassword(self):

        return self.properties.get('unicodepassword')
    
    @property
    def unixpassword(self):

        return self.properties.get('unixpassword')

    @property
    def userpassword(self):

        return self.properties.get('userpassword')


class BloodhoundArchive(object):

    def __init__(self, zf):

        self._dataset_cache = {}
        self._zf = zf

    @classmethod
    def open(cls, path):

        return cls(zipfile.ZipFile(path, mode='r'))
    
    @property
    def computers(self):

        return self._get_dataset('computers')
    
    @property
    def groups(self):

        return self._get_dataset('groups')
    
    @property
    def objects(self):

        return self.computers + self.groups + self.ous + self.users
    
    @property
    def ous(self):

        return self._get_dataset('ous')
    
    @property
    def users(self):

        return self._get_dataset('users')

    def get_dataset_for_principal_type(self, principal_type):

        if principal_type == 'Group':
            return self.groups

        elif principal_type == 'Computer':
            return self.computers

        elif principal_type == 'User':
            return self.users
        
        elif principal_type == 'OU':
            return self.ous
        
        else:
            raise ValueError(f"unknown principal type '{principal_type}'")
    
    def _get_dataset(self, dataset_name):

        match = None

        try:
            return self._dataset_cache[dataset_name]
        except KeyError:
            pass

        for file_info in self._zf.filelist:
            if re.match(JSON_FILENAME_TEMPLATE.format(dataset=dataset_name), file_info.filename):
                match = file_info
                break
        
        if match is None:
            raise ValueError(f"could not find dataset '{dataset_name}' in archive")
        
        with self._zf.open(file_info.filename, 'r') as f:
            dataset = [BloodhoundObject(record) for record in json.loads(f.read())['data']]
            self._dataset_cache[dataset_name] = dataset
            return dataset


arg_parser = argparse.ArgumentParser(prog='makima')
arg_parser.add_argument('-d', '--debug', action='store_true')
arg_parser.add_argument('command', choices=['aces', 'delegation', 'descriptions', 'laps', 'passwordage', 'passwords', 'pwdnotreqd'])
arg_parser.add_argument('bloodhound_archive')

args = arg_parser.parse_args()

if args.debug:
    logging.basicConfig(level=logging.DEBUG)

archive = BloodhoundArchive.open(args.bloodhound_archive)

if args.command == 'aces':

    controllers_by_principal_type = {}
    for computer in archive.objects:
        for ace in computer.aces:
            if ace.principal_type == 'Base':
                continue
            if ace.right in FUN_RIGHTS:
                controller_subjects = controllers_by_principal_type.setdefault(ace.principal_type, {}).setdefault(ace.sid, set())
                controller_subjects.add((computer.oid, computer.name, ace.right))

    table = []

    for principal_type, controllers in controllers_by_principal_type.items():
        for principal in archive.get_dataset_for_principal_type(principal_type):
            principal_oid = principal.oid
            try:
                subjects = controllers[principal_oid]
            except KeyError:
                pass
            else:
                for subject_oid, subject_name, right in subjects:
                    table.append((principal.name, subject_name, right))

    table.sort()

    print(tabulate.tabulate(table, tablefmt='plain'))

elif args.command == 'descriptions':

    table = []

    for obj in archive.objects:
        if obj.description is not None:
            table.append((obj.name, obj.description))
    
    table.sort()
    print(tabulate.tabulate(table, tablefmt='plain'))

elif args.command == 'delegation':

    table = []

    for obj in archive.objects:
        if obj.trusted_to_auth_for_delegation or obj.unconstrained_delegation_configured:
            delegation_type = 'constrained' if obj.trusted_to_auth_for_delegation else 'unconstrained'
            table.append((obj.name, delegation_type))

    print(tabulate.tabulate(table, tablefmt='plain'))

elif args.command == 'laps':

    rows = []
    for computer in archive.computers:
        if computer.enabled and not computer.has_laps:
            rows.append((computer.last_logon_timestamp, computer.name))
    rows.sort()
    print(tabulate.tabulate(rows, tablefmt='plain'))

elif args.command == 'passwordage':

    rows = []

    for obj in itertools.chain(archive.users, archive.computers):
        try:
            if obj.enabled:
                rows.append((obj.password_last_set, obj.name))
        except KeyError as e:
            logging.debug(f'error retrieving password age: {str(e)}')
    
    rows.sort()
    print(tabulate.tabulate(rows, tablefmt='plain'))

elif args.command == 'passwords':

    table = []

    for user in archive.users:
        if user.userpassword is not None or user.unixpassword is not None or user.unicodepassword is not None:
            table.append((user.name, (user.userpassword, user.unixpassword, user.unicodepassword)))
    
    table.sort()
    print(tabulate.tabulate(table, tablefmt='plain'))

elif args.command == 'pwdnotreqd':

    table = []

    for user in archive.users:
        if user.password_not_required is not None and user.password_not_required:
            table.append((user.name, user.description))
    
    table.sort()
    print(tabulate.tabulate(table, tablefmt='plain'))
